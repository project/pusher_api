<?php

namespace Drupal\Tests\pusher_api\Kernel\EventSubscriber;

use Drupal\pusher_api\DTO\Channels;
use Drupal\pusher_api\DTO\Data;
use Drupal\pusher_api\DTO\Event;
use Drupal\pusher_api\Event\TriggerEvent;
use Drupal\pusher_api\Service\PusherService;
use Drupal\Tests\pusher_api\Kernel\PusherTestBase;

/**
 * Tests the trigger event subscriber.
 *
 * @group pusher_api
 */
class TriggerEventSubscriberTest extends PusherTestBase {

  /**
   * Tests the trigger event subscriber.
   */
  public function testTriggerEventSubscriber() {
    $channels = new Channels(['channel1', 'channel2']);
    $event = new Event('testEvent');
    $data = new Data(['key' => 'value']);

    $pusherServiceMock = $this->createMock(PusherService::class);

    $pusherServiceMock->expects($this->once())
      ->method('trigger')
      ->with(
        $this->equalTo($channels),
        $this->equalTo($event),
        $this->equalTo($data)
      )
      ->willReturnCallback(function ($channelsArg, $eventArg, $dataArg) use ($channels, $event, $data) {
        $this->assertEquals($channels, $channelsArg, 'Channels do not match.');
        $this->assertEquals($event, $eventArg, 'Event does not match.');
        $this->assertEquals($data, $dataArg, 'Data does not match.');
      });

    $this->container->set('pusher_api.pusher.service.default', $pusherServiceMock);

    /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher */
    $dispatcher = $this->container->get('event_dispatcher');

    $triggerEvent = new TriggerEvent($channels, $event, $data);
    $dispatcher->dispatch($triggerEvent);
  }

}
