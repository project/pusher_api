<?php

namespace Drupal\Tests\pusher_api\Kernel;

use Drupal\pusher_api\Service\PusherService;

/**
 * Tests the Pusher API service provider.
 *
 * @group pusher_api
 */
class PusherApiServiceProviderTest extends PusherTestBase {

  /**
   * Tests service registration.
   */
  public function testServiceRegistration(): void {
    $serviceId = 'pusher_api.pusher.service.default';
    $this->assertTrue($this->container->has($serviceId), "The service '{$serviceId}' is not registered in the container.");

    $service = $this->container->get($serviceId);
    $this->assertInstanceOf(PusherService::class, $service, "The service '{$serviceId}' is not an instance of PusherService.");
  }

}
