<?php

namespace Drupal\Tests\pusher_api\Kernel;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Site\Settings;
use Drupal\KernelTests\KernelTestBase;
use Drupal\pusher_api\Service\PusherService;
use Pusher\Pusher;

/**
 * Tests the PusherService.
 *
 * @group pusher_api
 */
abstract class PusherTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['pusher_api', 'system'];

  /**
   * The Pusher service.
   *
   * @var \Drupal\pusher_api\Service\PusherService
   */
  protected PusherService $pusherService;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $pusherMock = $this->createMock(Pusher::class);
    $pusherMock->method('authorizeChannel')->willReturn('{"foo": "bar"}');

    $loggerChannelMock = $this->createMock(LoggerChannelInterface::class);
    $loggerChannelMock->expects($this->never())
      ->method('error');

    $this->pusherService = new PusherService($pusherMock, $loggerChannelMock);
  }

  /**
   * {@inheritDoc}
   */
  protected function bootEnvironment() {
    parent::bootEnvironment();

    $settings = Settings::getAll();
    $settings['pusher_api']['default'] = [
      'app_id' => 'xxxxx',
      'key' => 'xxxxx',
      'secret' => 'xxxx',
      'options' => [
        'cluster' => 'eu',
        'useTLS' => TRUE,
      ],
    ];

    new Settings($settings);
  }

}
