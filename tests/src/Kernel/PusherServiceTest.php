<?php

namespace Drupal\Tests\pusher_api\Kernel;

use Drupal\pusher_api\DTO\Channels;
use Drupal\pusher_api\DTO\Data;
use Drupal\pusher_api\DTO\Event;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Tests the PusherService.
 *
 * @group pusher_api
 */
class PusherServiceTest extends PusherTestBase {

  /**
   * Tests trigger.
   */
  public function testTrigger() {
    $channels = new Channels(['test_channel']);
    $event = new Event('test_event');
    $data = new Data(['key' => 'value']);

    $this->pusherService->trigger($channels, $event, $data);
  }

  /**
   * Tests auth.
   */
  public function testAuthorizeChannel() {
    $webSocketId = '12345';
    $data = new Data(['channel_name' => 'private-test']);

    $response = $this->pusherService->authorizeChannel($webSocketId, $data);

    $this->assertInstanceOf(JsonResponse::class, $response);
    $this->assertEquals(200, $response->getStatusCode());
  }

}
