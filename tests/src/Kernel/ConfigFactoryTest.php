<?php

namespace Drupal\Tests\pusher_api\Kernel\Factory;

use Drupal\pusher_api\DTO\Config;
use Drupal\pusher_api\Factory\ConfigFactory;
use Drupal\Tests\pusher_api\Kernel\PusherTestBase;

/**
 * Tests the ConfigFactory service.
 *
 * @group pusher_api
 */
class ConfigFactoryTest extends PusherTestBase {

  /**
   * Tests the creation of a Config object with valid settings.
   */
  public function testCreateConfig() {
    $fakeSettings = [
      'default' => [
        'app_id' => 'testAppId',
        'key' => 'testKey',
        'secret' => 'testSecret',
        'options' => ['cluster' => 'testCluster'],
      ],
    ];
    $this->setSetting('pusher_api', $fakeSettings);

    $configFactory = $this->container->get(ConfigFactory::class);
    $config = $configFactory->create();

    $this->assertInstanceOf(Config::class, $config);
    $this->assertEquals('testAppId', $config->appId);
  }

  /**
   * Tests behavior when required settings are missing.
   */
  public function testMissingSettings() {
    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('Pusher API settings not defined');

    // Simulate missing settings.
    $this->setSetting('pusher_api', []);

    $configFactory = $this->container->get(ConfigFactory::class);
    $configFactory->create();
  }

}
