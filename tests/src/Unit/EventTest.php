<?php

namespace Drupal\Tests\pusher_api\Unit;

use Drupal\pusher_api\DTO\Event;
use PHPUnit\Framework\TestCase;

/**
 * Event DTO tests.
 */
class EventTest extends TestCase {

  /**
   * Tests event name property.
   */
  public function testEventNameProperty(): void {
    $eventName = 'SampleEvent';

    $event = new Event($eventName);

    $this->assertEquals($eventName, $event->name, 'The name property does not match the expected value.');
  }

  /**
   * Tests to string magic method.
   */
  public function testToStringMethod() {
    $eventName = 'SampleEvent';

    $event = new Event($eventName);

    $this->assertEquals($eventName, (string) $event, 'The __toString method does not return the expected value.');
  }

}
