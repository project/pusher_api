<?php

namespace Drupal\Tests\pusher_api\Unit;

use Drupal\pusher_api\DTO\Config;
use Drupal\Tests\UnitTestCase;

/**
 * Tests config DTO.
 */
class ConfigTest extends UnitTestCase {

  /**
   * Tests config properties.
   */
  public function testConfigProperties(): void {
    $configKey = 'exampleConfigKey';
    $appId = 'exampleAppId';
    $key = 'exampleKey';
    $secret = 'exampleSecret';
    $options = ['cluster' => 'eu', 'useTLS' => TRUE];

    $config = new Config($configKey, $appId, $key, $secret, $options);

    $this->assertEquals($configKey, $config->configKey, 'The configKey does not match the expected value.');
    $this->assertEquals($appId, $config->appId, 'The appId does not match the expected value.');
    $this->assertEquals($key, $config->key, 'The key does not match the expected value.');
    $this->assertEquals($secret, $config->secret, 'The secret does not match the expected value.');
    $this->assertEquals($options, $config->options, 'The options array does not match the expected value.');
  }

}
