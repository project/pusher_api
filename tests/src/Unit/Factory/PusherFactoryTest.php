<?php

namespace Drupal\Tests\pusher_api\Unit\Factory;

use Drupal\pusher_api\DTO\Config;
use Drupal\pusher_api\Factory\PusherFactory;
use PHPUnit\Framework\TestCase;
use Pusher\Pusher;

/**
 * Tests PusherFactory.
 */
class PusherFactoryTest extends TestCase {

  /**
   * Tests create method.
   */
  public function testCreate(): void {
    $configKey = 'exampleConfigKey';
    $appId = 'exampleAppId';
    $key = 'exampleKey';
    $secret = 'exampleSecret';
    $options = ['cluster' => 'eu', 'useTLS' => TRUE];

    $config = new Config($configKey, $appId, $key, $secret, $options);

    $factory = new PusherFactory();

    $pusher = $factory->create($config);

    $this->assertInstanceOf(Pusher::class, $pusher);
  }

}
