<?php

namespace Drupal\Tests\pusher_api\Unit\Factory;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\pusher_api\DTO\Config;
use Drupal\pusher_api\Factory\ConfigFactory;
use Drupal\pusher_api\Factory\PusherFactory;
use Drupal\pusher_api\Factory\PusherServiceFactory;
use Drupal\pusher_api\Service\PusherService;
use PHPUnit\Framework\TestCase;
use Pusher\Pusher;

/**
 * Tests PusherServiceFactory.
 */
class PusherServiceFactoryTest extends TestCase {

  /**
   * Tests create method.
   */
  public function testCreate() {
    $configMock = $this->createMock(Config::class);
    $pusherMock = $this->createMock(Pusher::class);
    $loggerChannelFactoryMock = $this->createMock(LoggerChannelFactoryInterface::class);
    $loggerChannelMock = $this->createMock(LoggerChannelInterface::class);

    $configFactoryMock = $this->createMock(ConfigFactory::class);
    $configFactoryMock->method('create')->willReturn($configMock);

    $pusherFactoryMock = $this->createMock(PusherFactory::class);
    $pusherFactoryMock->method('create')->willReturn($pusherMock);

    $loggerChannelFactoryMock->method('get')->willReturn($loggerChannelMock);

    $pusherServiceFactory = new PusherServiceFactory(
      $configFactoryMock,
      $pusherFactoryMock,
      $loggerChannelFactoryMock
    );

    $pusherService = $pusherServiceFactory->create('default');

    $this->assertInstanceOf(PusherService::class, $pusherService);
  }

}
