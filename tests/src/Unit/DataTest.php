<?php

namespace Drupal\Tests\pusher_api\Unit;

use Drupal\pusher_api\DTO\Data;
use PHPUnit\Framework\TestCase;

/**
 * Data DTO tests.
 */
class DataTest extends TestCase {

  /**
   * Tests json serialization.
   */
  public function testJsonSerialize(): void {
    $sampleData = ['key' => 'value', 'number' => 123];

    $dataInstance = new Data($sampleData);

    $this->assertEquals(
      $sampleData,
      $dataInstance->jsonSerialize(),
      'The jsonSerialize method does not return the expected data.'
    );
  }

  /**
   * Tests json encoding.
   */
  public function testJsonEncode(): void {
    $sampleData = ['key' => 'value', 'number' => 123];
    $expectedJson = json_encode($sampleData);

    $dataInstance = new Data($sampleData);

    $this->assertJsonStringEqualsJsonString(
      $expectedJson,
      json_encode($dataInstance),
      'The JSON encoded string does not match the expected value.'
    );
  }

}
