<?php

namespace Drupal\Tests\pusher_api\Unit;

use Drupal\pusher_api\DTO\Channels;
use PHPUnit\Framework\TestCase;

/**
 * Channel DTO tests.
 */
class ChannelsTest extends TestCase {

  /**
   * Tests array access.
   */
  public function testArrayAccess(): void {
    $channels = new Channels();
    $channels['test'] = 'value';

    $this->assertEquals('value', $channels['test'], 'Array access does not work as expected.');
  }

  /**
   * Tests iterator.
   */
  public function testIteration(): void {
    $data = [
      'channel1' => 'value1',
      'channel2' => 'value2',
    ];

    $channels = new Channels($data);

    $collectedData = [];
    foreach ($channels as $key => $value) {
      $collectedData[$key] = $value;
    }

    $this->assertEquals($data, $collectedData, 'Iteration over Channels does not work as expected.');
  }

  /**
   * Tests countability.
   */
  public function testCountable(): void {
    $data = [
      'channel1' => 'value1',
      'channel2' => 'value2',
    ];

    $channels = new Channels($data);

    $this->assertCount(2, $channels, 'Channels does not count elements as expected.');
  }

}
