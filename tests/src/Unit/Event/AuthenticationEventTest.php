<?php

namespace Drupal\Tests\pusher_api\Unit\Event;

use Drupal\pusher_api\DTO\Data;
use Drupal\pusher_api\Event\AuthenticationEvent;
use PHPUnit\Framework\TestCase;

/**
 * Tests authentication event.
 */
class AuthenticationEventTest extends TestCase {

  /**
   * Tests getters and setters.
   */
  public function testEventGetterAndSetter(): void {
    $initialData = new Data(['key' => 'initialValue']);
    $event = new AuthenticationEvent($initialData);

    $this->assertSame($initialData, $event->getData(), 'Initial data does not match.');

    $newData = new Data(['key' => 'newValue']);
    $event->setData($newData);

    $this->assertSame($newData, $event->getData(), 'Updated data does not match.');
    $this->assertNotSame($initialData, $event->getData(), 'Data was not updated correctly.');
  }

}
