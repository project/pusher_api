<?php

namespace Drupal\Tests\pusher_api\Unit\Event;

use Drupal\pusher_api\DTO\Channels;
use Drupal\pusher_api\DTO\Data;
use Drupal\pusher_api\DTO\Event;
use Drupal\pusher_api\Event\TriggerEvent;
use PHPUnit\Framework\TestCase;

/**
 * Tests trigger event.
 */
class TriggerEventTest extends TestCase {

  /**
   * Testing trigger event properties.
   */
  public function testTriggerEventProperties(): void {
    $channels = new Channels(['channel1', 'channel2']);
    $event = new Event('eventName');
    $data = new Data(['key' => 'value']);

    $triggerEvent = new TriggerEvent($channels, $event, $data);

    $this->assertSame($channels, $triggerEvent->channels, 'Channels property does not match the expected object.');
    $this->assertSame($event, $triggerEvent->event, 'Event property does not match the expected object.');
    $this->assertSame($data, $triggerEvent->data, 'Data property does not match the expected object.');
  }

}
