<?php

namespace Drupal\pusher_api\Event;

use Drupal\pusher_api\DTO\Channels;
use Drupal\pusher_api\DTO\Data;
use Drupal\pusher_api\DTO\Event;
use Symfony\Contracts\EventDispatcher\Event as SymfonyEvent;

/**
 * Represents an event that triggers actions across specified channels.
 */
class TriggerEvent extends SymfonyEvent {

  /**
   * Constructs a new TriggerEvent object.
   *
   * @param \Drupal\pusher_api\DTO\Channels $channels
   *   The channels through which the event is to be triggered.
   * @param \Drupal\pusher_api\DTO\Event $event
   *   The event to trigger.
   * @param \Drupal\pusher_api\DTO\Data $data
   *   The data associated with the event.
   */
  public function __construct(
    public readonly Channels $channels,
    public readonly Event $event,
    public readonly Data $data,
  ) {
  }

}
