<?php

namespace Drupal\pusher_api\Event;

use Drupal\pusher_api\DTO\Data;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the AuthenticationEvent class.
 *
 * This event is used to manage authentication data within the Pusher API.
 */
class AuthenticationEvent extends Event {

  /**
   * Constructs a new AuthenticationEvent object.
   *
   * @param \Drupal\pusher_api\DTO\Data $data
   *   The data transfer object for authentication data.
   */
  public function __construct(
    protected Data $data
  ) {
  }

  /**
   * Gets the authentication data.
   *
   * @return \Drupal\pusher_api\DTO\Data
   *   The data transfer object containing authentication data.
   */
  public function getData(): Data {
    return $this->data;
  }

  /**
   * Sets the authentication data.
   *
   * @param \Drupal\pusher_api\DTO\Data $data
   *   The new data transfer object for authentication data.
   */
  public function setData(Data $data): void {
    $this->data = $data;
  }

}
