<?php

namespace Drupal\pusher_api\DTO;

/**
 * Event class represents the event sent to Pusher.
 */
class Event {

  /**
   * Constructor.
   *
   * @param string $name
   *   Name of the event.
   */
  public function __construct(
    public readonly string $name,
  ) {
  }

  /**
   * To string magic method.
   */
  public function __toString(): string {
    return $this->name;
  }

}
