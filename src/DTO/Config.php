<?php

namespace Drupal\pusher_api\DTO;

/**
 * Configuration class representing Pusher config.
 */
class Config {

  /**
   * Constructor.
   *
   * @param string $configKey
   *   Configuration key.
   * @param string $appId
   *   Application ID.
   * @param string $key
   *   Pusher key.
   * @param string $secret
   *   Pusher secret.
   * @param array $options
   *   Pusher options.
   */
  public function __construct(
    public readonly string $configKey,
    public readonly string $appId,
    public readonly string $key,
    public readonly string $secret,
    public readonly array $options,
  ) {
  }

}
