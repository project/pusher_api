<?php

namespace Drupal\pusher_api\DTO;

/**
 * Data class representing data sent to Pusher.
 */
class Data implements \JsonSerializable {

  /**
   * Constructor.
   *
   * @param array $data
   *   Data.
   */
  public function __construct(
    public readonly array $data,
  ) {
  }

  /**
   * {@inheritDoc}
   */
  public function jsonSerialize(): array {
    return $this->data;
  }

}
