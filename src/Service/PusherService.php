<?php

namespace Drupal\pusher_api\Service;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\pusher_api\DTO\Channels;
use Drupal\pusher_api\DTO\Data;
use Drupal\pusher_api\DTO\Event;
use Pusher\Pusher;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Service class for interacting with the Pusher library.
 */
class PusherService {

  /**
   * Constructs a new PusherService object.
   *
   * @param \Pusher\Pusher $pusher
   *   The Pusher library instance.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   The logger channel for logging messages.
   */
  public function __construct(
    protected Pusher $pusher,
    protected LoggerChannelInterface $loggerChannel,
  ) {
  }

  /**
   * Triggers an event on the given channels with the provided data.
   *
   * @param \Drupal\pusher_api\DTO\Channels $channels
   *   The channels to trigger the event on.
   * @param \Drupal\pusher_api\DTO\Event $event
   *   The event to trigger.
   * @param \Drupal\pusher_api\DTO\Data $data
   *   The data to send with the event.
   */
  public function trigger(Channels $channels, Event $event, Data $data): void {
    try {
      $this->pusher->trigger(
        $channels->getArrayCopy(),
        $event,
        $data,
      );
    }
    catch (\Throwable $throwable) {
      $this->loggerChannel->error('pusher_api: ' . $throwable->getMessage());
    }
  }

  /**
   * Authorizes a subscription request for a private or presence channel.
   *
   * @param string $webSocketId
   *   The WebSocket ID associated with the subscription request.
   * @param \Drupal\pusher_api\DTO\Data $data
   *   The data required for authorization, including the channel name.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response to the authorization request.
   */
  public function authorizeChannel(string $webSocketId, Data $data): JsonResponse {
    try {
      return new JsonResponse(
        data: $this->pusher->authorizeChannel($data->data['channel_name'], $webSocketId),
        json: TRUE,
      );
    }
    catch (\Throwable $throwable) {
      $this->loggerChannel->error('pusher_api: ' . $throwable->getMessage());

      return new JsonResponse('Something went wrong...', 500);
    }
  }

}
