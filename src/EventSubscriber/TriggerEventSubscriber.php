<?php

namespace Drupal\pusher_api\EventSubscriber;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\pusher_api\Event\TriggerEvent;
use Drupal\pusher_api\Service\PusherService;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for TriggerEvent.
 */
class TriggerEventSubscriber implements EventSubscriberInterface {

  use AutowireTrait;

  /**
   * Constructs a new TriggerEventSubscriber.
   *
   * @param \Drupal\pusher_api\Service\PusherService $pusherService
   *   The Pusher service.
   */
  public function __construct(
    #[Autowire(service: 'pusher_api.pusher.service.default')]
    protected PusherService $pusherService,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      TriggerEvent::class => [
        ['trigger', -100],
      ],
    ];
  }

  /**
   * Handles the trigger event.
   *
   * @param \Drupal\pusher_api\Event\TriggerEvent $event
   *   The event to handle.
   */
  public function trigger(TriggerEvent $event) {
    $this->pusherService->trigger(
      $event->channels,
      $event->event,
      $event->data,
    );
  }

}
