<?php

namespace Drupal\pusher_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\pusher_api\DTO\Data;
use Drupal\pusher_api\Event\AuthenticationEvent;
use Drupal\pusher_api\Service\PusherService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Authentication controller used for authenticating Pusher users.
 */
class AuthenticationController extends ControllerBase {

  use AutowireTrait;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   * @param \Drupal\pusher_api\Service\PusherService $pusherService
   *   Pusher service.
   */
  public function __construct(
    protected EventDispatcherInterface $eventDispatcher,
    protected PusherService $pusherService,
  ) {
  }

  /**
   * Authentication callback used to authenticate Pusher users.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response in JSON.
   */
  public function authenticate(Request $request): JsonResponse {
    try {
      if ($this->currentUser()->isAuthenticated() == FALSE) {
        return new JsonResponse('Forbidden', 403);
      }

      if (empty($request->request->get('socket_id'))) {
        return new JsonResponse(['error' => 'Socket ID not found.'], 400);
      }

      $data = new Data(
        [
          'id' => (string) $this->currentUser->id(),
          'channel_name' => $request->request->get('channel_name'),
        ]
      );

      $event = $this->eventDispatcher->dispatch(new AuthenticationEvent($data));

      $data = $event->getData();

      return $this->pusherService->authorizeChannel(
        $request->request->get('socket_id'),
        $data,
      );
    }
    catch (\Throwable $throwable) {
      $this->getLogger('pusher_api.controller.login')->error($throwable->getMessage());

      return new JsonResponse(['Something went wrong...']);
    }
  }

}
