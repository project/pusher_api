<?php

namespace Drupal\pusher_api\Factory;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\pusher_api\Service\PusherService;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Factory for creating PusherService instances.
 */
class PusherServiceFactory {

  use AutowireTrait;

  /**
   * Constructs a new PusherServiceFactory object.
   *
   * @param ConfigFactory $configFactory
   *   The factory used to create Config instances.
   * @param PusherFactory $pusherFactory
   *   The factory used to create Pusher instances.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   */
  public function __construct(
    protected ConfigFactory $configFactory,
    protected PusherFactory $pusherFactory,
    #[Autowire(service: 'logger.factory')]
    protected LoggerChannelFactoryInterface $loggerChannelFactory,
  ) {
  }

  /**
   * Creates a new PusherService instance.
   *
   * @param string $key
   *   The configuration key to use for creating the service.
   *
   * @return \Drupal\pusher_api\Service\PusherService
   *   The created PusherService instance.
   */
  public function create(string $key): PusherService {
    $config = $this->configFactory->create($key);
    $pusher = $this->pusherFactory->create($config);
    $logger = $this->loggerChannelFactory->get('pusher_api.pusher.service.' . $key);
    $pusher->setLogger($logger);

    return new PusherService(
      $pusher,
      $logger,
    );
  }

}
