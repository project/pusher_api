<?php

namespace Drupal\pusher_api\Factory;

use Drupal\pusher_api\DTO\Config;
use Pusher\Pusher;

/**
 * Factory for creating instances of the Pusher class.
 */
class PusherFactory {

  /**
   * Creates a new instance of the Pusher class using provided configuration.
   *
   * @param \Drupal\pusher_api\DTO\Config $config
   *   The configuration DTO containing the options to initialize Pusher.
   *
   * @return \Pusher\Pusher
   *   An instance of the Pusher class configured with provided settings.
   */
  public function create(Config $config): Pusher {
    return new Pusher($config->key, $config->secret, $config->appId, $config->options);
  }

}
