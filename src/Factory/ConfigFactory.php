<?php

namespace Drupal\pusher_api\Factory;

use Drupal\Core\Site\Settings;
use Drupal\pusher_api\DTO\Config;

/**
 * Factory for creating Config instances for Pusher API.
 */
class ConfigFactory {

  /**
   * Constructs a new ConfigFactory object.
   *
   * @param \Drupal\Core\Site\Settings $settings
   *   The Drupal site settings service.
   */
  public function __construct(
    protected Settings $settings
  ) {
  }

  /**
   * Retrieves the list of keys for all Pusher API configurations.
   *
   * @return array
   *   An array of configuration keys.
   */
  public function keys(): array {
    return array_keys($this->get());
  }

  /**
   * Creates a new Config instance for the specified key.
   *
   * @param string $key
   *   The configuration key, defaults to 'default'.
   *
   * @return \Drupal\pusher_api\DTO\Config
   *   The created Config instance.
   *
   * @throws \Exception
   *   Throws exception if the configuration is not defined or invalid.
   */
  public function create(string $key = 'default'): Config {
    $settings = $this->get();

    if (empty($settings[$key])) {
      throw new \Exception('Pusher API settings for "' . $key . '" are not defined, please check pusher_api documentation.');
    }

    if (!is_array($settings[$key])) {
      throw new \Exception('Pusher API channel settings must be an array, please check pusher_api documentation.');
    }

    $channelSettings = $settings[$key];

    if (!array_key_exists('options', $channelSettings) || !is_array($channelSettings['options'])) {
      throw new \Exception('Pusher API channel "options" must be an array, please check pusher_api documentation.');
    }

    return new Config(
      $key,
      $channelSettings['app_id'],
      $channelSettings['key'],
      $channelSettings['secret'],
      $channelSettings['options'],
    );
  }

  /**
   * Get all configuration keys from settings.
   *
   * @return array
   *   List of config keys.
   *
   * @throws \Exception
   */
  private function get(): array {
    $settings = $this->settings->get('pusher_api');

    if (!is_array($settings) || empty($settings)) {
      throw new \Exception('Pusher API settings not defined, please check pusher_api documentation.');
    }

    return $settings;
  }

}
