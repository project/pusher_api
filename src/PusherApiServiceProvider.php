<?php

namespace Drupal\pusher_api;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Site\Settings;
use Drupal\pusher_api\Factory\PusherServiceFactory;
use Drupal\pusher_api\Service\PusherService;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Service provider for pusher_api.
 */
class PusherApiServiceProvider extends ServiceProviderBase {

  /**
   * Dynamically register PusherServices based on settings provided.
   */
  public function register(ContainerBuilder $container): void {
    $configSettngs = Settings::get('pusher_api');
    if (!is_null($configSettngs) && !is_array($configSettngs)) {
      throw new \Exception('Pusher API settings must be an array, please check pusher_api documentation.');
    }

    if (is_array($configSettngs)) {
      foreach ($configSettngs as $key => $config) {
        $container->register('pusher_api.pusher.service.' . $key, PusherService::class)
          ->setFactory([new Reference(PusherServiceFactory::class), 'create'])
          ->addArgument($key);
      }
    }
  }

}
